
local function createdStar()
  local stars = display.newGroup()
  local starpart1 = display.newLine( 150, 100, 50, 300 )
  starpart1:append( 150, 100, 250 , 300 , 50, 300 ,150, 100)
  starpart1.stroke = { 1, 0.5, 0, 1 }
  starpart1.fill = { 0, 0.5, 0, 1 }
  starpart1.strokeWidth = 18

  local starpart2 = display.newLine( 50,175,150,375)
  starpart2:append( 50,175, 250,175,150,375,50,175)
  starpart2.stroke = { 1, 0.5, 0, 1 }
  starpart2.fill = { 0, 0.5, 0, 1 }
  starpart2.strokeWidth = 18
  stars:insert(starpart1)
  stars:insert(starpart2)
  return stars
end
 stars1 = createdStar()
 stars2 = createdStar()
 stars3 = createdStar()
 stars4 = createdStar()
 stars5 = createdStar()
local function touchEventListener(event)


	if event.phase == "began" then
    transition.to(stars1, { time = 1000, x = display.contentWidth,y = display.contentHeight,alpha = 0})
    transition.to(stars2, { time = 1000, x = display.contentWidth,y = -stars2.width,alpha = 0})
    transition.to(stars3, { time = 1000, x = -stars2.height,y = display.contentHeight,alpha = 0})
    transition.to(stars4, { time = 1000, x = -stars2.height,y = -stars2.width,alpha = 0})
    transition.to(stars5, { time = 1000, x = -stars2.height,y = -stars2.width,alpha = 0})
  end
end

Runtime:addEventListener("touch", touchEventListener)
